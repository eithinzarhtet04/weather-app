import 'package:flutter/material.dart';

String iconLink(String code){
  debugPrint('https://openweathermap.org/img/wn/$code@2x.png');
  return 'https://openweathermap.org/img/wn/$code@2x.png';
}