class ApiConst{
  static const String baseUrl = 'https://api.openweathermap.org/';
  static const String citySearch = 'geo/1.0/direct';
  static const String currentWeather = 'data/2.5/weather';
  static const String apiKey = '4fa9da15c015cb6460ecbd5236bf90d0';
  static const String units = 'metric';

}