part of 'weather_detail_bloc.dart';

@immutable
sealed class WeatherDetailState {}

final class WeatherDetailInitial extends WeatherDetailState {}

class WeatherDetailLoading extends WeatherDetailState{}

class WeatherDetailSuccess extends WeatherDetailState{
  // ignore: unused_field
  final CurrentWeatherModel currentWeatherModel;

  WeatherDetailSuccess(this.currentWeatherModel);
}

class WeatherDetailFail extends WeatherDetailState{
  final String errorMessage;

  WeatherDetailFail(this.errorMessage);
}