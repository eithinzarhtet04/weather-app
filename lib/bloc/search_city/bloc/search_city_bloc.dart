import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:weather_with_bloc/data/model/city_search_model.dart';
import 'package:weather_with_bloc/data/weather_repository.dart';

part 'search_city_event.dart';
part 'search_city_state.dart';

class SearchCityBloc extends Bloc<SearchCity, SearchCityState> {
  final WeatherRepository _weatherRepository = WeatherRepository();
  SearchCityBloc() : super(SearchCityInitial()) {
    on<SearchCity>((event, emit) async{
        try{
          emit(SearchCityLoading());
          List<CitySearchModel> cities = await _weatherRepository.searchCity(city: event.city);          
          emit(SearchCitySuccess(cities));
        }
        catch(e){
          emit(SearchCityFailed(e.toString()));
        }
    });
  }
}
