import 'package:flutter/material.dart';
import 'package:weather_with_bloc/ui/city_search/city_search.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      showPerformanceOverlay: true,
      debugShowCheckedModeBanner: false,
      home: CitySearchScreen(),
    );
  }
}
