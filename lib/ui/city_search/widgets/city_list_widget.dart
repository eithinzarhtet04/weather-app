import 'package:flutter/material.dart';
import 'package:weather_with_bloc/data/model/city_search_model.dart';
import 'package:weather_with_bloc/ui/weather_detail/weather_detail_screen.dart';

class CountryListWidget extends StatelessWidget {
  const CountryListWidget({
    Key? key,
    required this.cities,
  }) : super(key: key);

  final List<CitySearchModel> cities;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
          itemCount: cities.length,
          itemBuilder: (context, index) {
            CitySearchModel city = cities[index];
            return ListTile(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (_) => WeatherDetailScreen(lat: city.lat ?? 0, lon: city.lon ?? 0,)));
              },
              title: Text(city.name ?? ''),
              subtitle: Text(city.country ?? ''),
            );
          }),
    );
  }
}