import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:weather_with_bloc/const/icon_const.dart';
import 'package:weather_with_bloc/data/model/current_weather_model.dart';

Widget temperatureWidget(CurrentWeatherModel currentWeatherModel, Weather weather){
  return SizedBox(
    height: 300,
    child: Row(
      children: [
        Expanded(
          child: CachedNetworkImage(
            imageUrl: iconLink(weather.icon ?? '')
          ),
        ),
        Expanded(
          child: Text('${currentWeatherModel.main!.temp} °',
          style: const TextStyle(color: Colors.white, fontSize: 35,),),
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Max: ${currentWeatherModel.main!.tempMax}', style: const TextStyle(color: Colors.white, fontSize: 20,)),
              const SizedBox(height: 5),
              Text('Min: ${currentWeatherModel.main!.tempMin}', style: const TextStyle(color: Colors.white, fontSize: 20,)),
            ],
          ),
        )
      ],
    ),
  );
}
