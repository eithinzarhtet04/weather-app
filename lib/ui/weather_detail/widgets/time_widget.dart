import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

Widget timeWidget(int time){
  DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(time * 1000);
  final format = DateFormat.jm();
  return Text('Update: ${format.format(dateTime)}',
    style: const TextStyle(color: Colors.white, fontSize: 25,),);
}
