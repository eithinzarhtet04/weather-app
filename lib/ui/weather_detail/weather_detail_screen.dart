import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:weather_with_bloc/bloc/weather_detail/bloc/weather_detail_bloc.dart';
import 'package:weather_with_bloc/const/icon_const.dart';
import 'package:weather_with_bloc/data/model/current_weather_model.dart';

import 'widgets/temperature_widget.dart';
import 'widgets/time_widget.dart';


class WeatherDetailScreen extends StatefulWidget {
  const WeatherDetailScreen({super.key, required this.lat, required this.lon});
  final double lat;
  final double lon;

  @override
  State<WeatherDetailScreen> createState() => _WeatherDetailScreenState();
}

class _WeatherDetailScreenState extends State<WeatherDetailScreen> {
  final WeatherDetailBloc _weatherDetailBloc = WeatherDetailBloc();
  @override
  void initState() {
    super.initState();
    _weatherDetailBloc.add(WeatherDetail(widget.lat, widget.lon));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _weatherDetailBloc,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor:const Color(0xff283593),
          elevation: 0,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                Color(0xff5C6BC0),
                Color(0xff3949AB),
                Color(0xff283593)
              ])),
          child: BlocBuilder<WeatherDetailBloc, WeatherDetailState>(
            builder: (context, state) {
              if (state is WeatherDetailLoading) {
                return const Center(child: CircularProgressIndicator());
              } else if (state is WeatherDetailSuccess) {
                CurrentWeatherModel currentWeatherModel = state.currentWeatherModel;
                Weather weather = currentWeatherModel.weather![0];
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      currentWeatherModel.name ?? '',
                      style:
                          const TextStyle(color: Colors.white, fontSize: 35, fontWeight: FontWeight.bold),),
                          const SizedBox(
                            height: 10,
                          ),
                          timeWidget(currentWeatherModel.dt ?? 0), 
                          temperatureWidget(currentWeatherModel, weather),
                          Text(weather.main ?? '',
                          style: const TextStyle(color: Colors.white, fontSize: 30),)
                  ],
                );
              } else if (state is WeatherDetailFail) {
                return Center(child: Text(state.errorMessage));
              }
              return const SizedBox();
            },
          ),
        ),
      ),
    );
  }
}
