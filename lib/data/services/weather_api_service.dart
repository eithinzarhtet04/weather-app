import 'package:retrofit/http.dart';
import 'package:dio/dio.dart';
import 'package:weather_with_bloc/const/api_const.dart';
import 'package:weather_with_bloc/data/model/city_search_model.dart';
import 'package:weather_with_bloc/data/model/current_weather_model.dart';
part 'weather_api_service.g.dart';

@RestApi(baseUrl: ApiConst.baseUrl)
abstract class WeatherApiService{
  factory WeatherApiService(Dio dio) => _WeatherApiService(dio);

  @GET(ApiConst.citySearch)
  Future<List<CitySearchModel>> searchCity({@Query('q') required String city, @Query('limit') required int limit, @Query('appid') required String appid});

  @GET(ApiConst.currentWeather)
  Future<CurrentWeatherModel> getWeatherList({@Query('lat') required double lat, @Query('lon') required double lon, @Query('appid') required String appid, @Query('units') required String units});
}