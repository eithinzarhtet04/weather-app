import 'package:dio/dio.dart';
import 'package:weather_with_bloc/const/api_const.dart';
import 'package:weather_with_bloc/data/model/city_search_model.dart';
import 'package:weather_with_bloc/data/model/current_weather_model.dart';
import 'package:weather_with_bloc/data/services/weather_api_service.dart';

class WeatherRepository {
  WeatherApiService? _weatherApiService;
  String? _appId;

  WeatherRepository() {
    final Dio dio = Dio();
    _appId = ApiConst.apiKey;
    _weatherApiService = WeatherApiService(dio);
  }

  Future<List<CitySearchModel>> searchCity({required String city}) =>
      _weatherApiService!.searchCity(city: city, limit: 10, appid: _appId!);

  Future<CurrentWeatherModel> getCurrentWeather(
          {required double lat, required double lon}) =>
      _weatherApiService!.getWeatherList(
          lat: lat, lon: lon, appid: _appId!, units: ApiConst.units);
}