class CitySearchModel {
  String? name;
  LocalNames? localNames;
  double? lat;
  double? lon;
  String? country;
  String? state;

  CitySearchModel(
      {this.name,
      this.localNames,
      this.lat,
      this.lon,
      this.country,
      this.state});

  CitySearchModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    localNames = json['local_names'] != null
        ? new LocalNames.fromJson(json['local_names'])
        : null;
    lat = json['lat'];
    lon = json['lon'];
    country = json['country'];
    state = json['state'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    if (this.localNames != null) {
      data['local_names'] = this.localNames!.toJson();
    }
    data['lat'] = this.lat;
    data['lon'] = this.lon;
    data['country'] = this.country;
    data['state'] = this.state;
    return data;
  }
}

class LocalNames {
  String? vi;
  String? sk;
  String? hi;
  String? de;
  String? uk;
  String? ru;
  String? ja;
  String? fr;
  String? bn;
  String? oc;
  String? cs;
  String? ko;
  String? eu;
  String? zh;
  String? da;
  String? ascii;
  String? featureName;
  String? en;
  String? my;
  String? et;
  String? pl;
  String? ku;
  String? or;
  String? lt;
  String? it;
  String? kn;
  String? es;

  LocalNames(
      {this.vi,
      this.sk,
      this.hi,
      this.de,
      this.uk,
      this.ru,
      this.ja,
      this.fr,
      this.bn,
      this.oc,
      this.cs,
      this.ko,
      this.eu,
      this.zh,
      this.da,
      this.ascii,
      this.featureName,
      this.en,
      this.my,
      this.et,
      this.pl,
      this.ku,
      this.or,
      this.lt,
      this.it,
      this.kn,
      this.es});

  LocalNames.fromJson(Map<String, dynamic> json) {
    vi = json['vi'];
    sk = json['sk'];
    hi = json['hi'];
    de = json['de'];
    uk = json['uk'];
    ru = json['ru'];
    ja = json['ja'];
    fr = json['fr'];
    bn = json['bn'];
    oc = json['oc'];
    cs = json['cs'];
    ko = json['ko'];
    eu = json['eu'];
    zh = json['zh'];
    da = json['da'];
    ascii = json['ascii'];
    featureName = json['feature_name'];
    en = json['en'];
    my = json['my'];
    et = json['et'];
    pl = json['pl'];
    ku = json['ku'];
    or = json['or'];
    lt = json['lt'];
    it = json['it'];
    kn = json['kn'];
    es = json['es'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['vi'] = this.vi;
    data['sk'] = this.sk;
    data['hi'] = this.hi;
    data['de'] = this.de;
    data['uk'] = this.uk;
    data['ru'] = this.ru;
    data['ja'] = this.ja;
    data['fr'] = this.fr;
    data['bn'] = this.bn;
    data['oc'] = this.oc;
    data['cs'] = this.cs;
    data['ko'] = this.ko;
    data['eu'] = this.eu;
    data['zh'] = this.zh;
    data['da'] = this.da;
    data['ascii'] = this.ascii;
    data['feature_name'] = this.featureName;
    data['en'] = this.en;
    data['my'] = this.my;
    data['et'] = this.et;
    data['pl'] = this.pl;
    data['ku'] = this.ku;
    data['or'] = this.or;
    data['lt'] = this.lt;
    data['it'] = this.it;
    data['kn'] = this.kn;
    data['es'] = this.es;
    return data;
  }
}
